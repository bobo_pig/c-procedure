#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>
//变种水仙花数
int main()
{
    for (int i = 10000; i <= 99999; i++)
    {
        int sum = 0;
        for (int j = 4; j > 0; j--)
        {
            sum += (i / (int)pow(10, j)) * (i % (int)pow(10, j));
        }
        if (sum == i)
            printf("%d ", i);
    }

    return 0;
}