#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//memcpyģ��
void* my_memcpy(void* dest, const void* src, size_t num)
{
    while (num--)
    {
        *(char*)dest = *(char*)src;
        dest = (char*)dest + 1;
        src = (char*)src + 1;
    }
}

int main()
{
    int arr1[] = { 7,2,1,3,8,9,6,0,5,4 };
    int arr2[10] = { 0 };
    char arr3[] = "hello world!";
    char arr4[10];
    my_memcpy(arr2, arr1, 20);
    for (int i = 0; i < 10; i++)
        printf("%d", arr2[i]);

    printf("\n");

    my_memcpy(arr4, arr3, 5);
    printf("%s", arr4);

    return 0;
}