#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//memmoveģ��
void* my_memmove(void* dest, const void* src, size_t num)
{
    void* ret = dest;
    while (num--)
    {
        if (dest < src)
        {
            *(char*)dest = *(char*)src;
            dest = (char*)dest + 1;
            src = (char*)src + 1;
        }
        else
        {
            *((char*)dest + num) = *((char*)src + num);
        }
    }
    return ret;
}

int main()
{
    int arr1[] = { 0,1,2,3,4,5,6,7,8,9 };
    char arr2[] = "hello world!";

    my_memmove(arr1 + 2, arr1, 20);
    for (int i = 0; i < 10; i++)
        printf("%d", arr1[i]);

    printf("\n");

    my_memmove(arr2 + 2, arr2, 5);
    printf("%s", arr2);

    return 0;
}