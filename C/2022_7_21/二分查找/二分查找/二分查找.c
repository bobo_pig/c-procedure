#include <stdio.h>//二分查找数组元素
int my_search(int* str, int s, int n)
{
    int left = 0;
    int right = s - 1;
    int mid = 0;
    while (left <= right)
    {
        mid = (left + right) / 2;
        if (str[mid] < n)
            left = mid + 1;
        else if (str[mid] > n)
            right = mid - 1;
        else
            return mid;
    }
    return 0;
}

int main()
{
    int key;
    scanf_s("%d", &key);
    int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
    int sz = sizeof(arr) / sizeof(arr[0]);
    int serial = my_search(arr, sz, key);
    if (serial)
        printf("值为%d的数组下标为%d", key, serial);
    else
        printf("找不到");

    return 0;
}