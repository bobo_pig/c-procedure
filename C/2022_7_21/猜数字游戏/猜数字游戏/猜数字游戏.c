#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
//猜数字游戏
void menu(void)
{
    printf("*********************\n"
        "******1. play *******\n"
        "******0. exit *******\n"
        "*********************\n");
    printf("请输入：>");
}

void game(void)
{
    printf("猜猜1~100之间的数字~!\n");
    int guess = 0;
    int ret = 0;
    ret = rand() % 100 - 1;

    while (1)
    {
        scanf("%d", &guess);
        if (guess > ret)
            printf("大了大了~~\n");
        else if (guess < ret)
            printf("小了小了~~\n");
        else
        {
            printf("恭喜你！答对了~~\n");
            break;
        }

    }
}

int main()
{
    srand((unsigned int)time(NULL));
    int input = 0;


    do
    {
        menu();
        scanf("%d", &input);

        switch (input)
        {
        case 1:
            game();
            break;
        case 0:
            printf("退出游戏！\n");
            break;
        default:
            printf("选择错误，请重新选择！\n");
            break;
        }
    } while (input);

    return 0;
}