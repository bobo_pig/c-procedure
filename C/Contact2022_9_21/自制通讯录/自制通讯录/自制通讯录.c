#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>//自制通讯录
#include<stdlib.h>
#include<errno.h>
#include<assert.h>
#include<string.h>
#include<conio.h>

#define _MAX 20
#define DEFAULT_SIZE 2

//用户信息
struct PeoInfo
{
    char name[_MAX];
    char sex[_MAX];
    int age;
    char tele[_MAX];
    char addr[_MAX];
};
//定义通讯录
struct Contact
{
    struct PeoInfo* data;
    int sz;
    int capacity;
};
//按名字查找
int FindByName(struct Contact* p, char* Name)
{
    for (int i = 0; i < p->sz; i++)
    {
        if (strcmp(p->data[i].name, Name) == 0)
            return i;
    }
    return -1;
}
//按名字排序
int SortByName(const void* e1, const void* e2)
{
    return strcmp((char*)e1, (char*)e2);
}
//检查扩容
int CheckCapacity(struct Contact* p)
{
    if (p->sz == p->capacity)
    {
        struct PeoInfo* ptr = (struct PeoInfo*)realloc(p->data, DEFAULT_SIZE * sizeof(struct PeoInfo));
        if (ptr == NULL)
        {
            perror("CheckCapacity()");
            return 0;
        }
        else
        {
            p->data = ptr;
            p->capacity = DEFAULT_SIZE * p->capacity;
            printf("扩容成功！\n");
            return 1;
        }
    }
    else
        return 1;
}
//保存数据到文件
void SaveContact(struct Contact* p)
{
    FILE* pfWrite = fopen("contact.txt", "wb");
    if (pfWrite == NULL)
    {
        perror("SaveContact");
        return;
    }
    for (int i = 0; i < p->sz; i++)
    {
        fwrite(p->data + i, sizeof(struct PeoInfo), 1, pfWrite);
    }
    fclose(pfWrite);
    pfWrite = NULL;
}
//从文件中读取数据
void LoadContact(struct Contact* p)
{
    FILE* pfRead = fopen("contact.txt", "rb");
    if (pfRead == NULL)
    {
        perror("LoadContact");
        return;
    }
    struct PeoInfo tmp = { 0 };
    while (fread(&tmp, sizeof(struct PeoInfo), 1, pfRead) == 1)
    {
        CheckCapacity(p);
        p->data[p->sz] = tmp;
        p->sz++;
    }
}
//初始化通讯录
void InitContact(struct Contact* p)
{
    p->data = (struct PeoInfo*)malloc(DEFAULT_SIZE * sizeof(struct PeoInfo));
    if (p->data == NULL)
    {
        perror("初始化失败：");
        return;
    }
    p->sz = 0;
    p->capacity = DEFAULT_SIZE;

    LoadContact(p);
    printf("初始化成功！\n");
}
//打印菜单
void menu()
{
    printf("\n");
    printf("    1.Add     2.Del\n");
    printf("    3.Search  4.Modify\n");
    printf("    5.Show    6.Sort\n");
    printf("    0.Exit\n");
    printf("\n");
}

//增加联系人
void Add(struct Contact* p)
{
    assert(p);

    int ret = CheckCapacity(p);
    if (ret)
    {
        printf("请输入姓名:>"); scanf("%s", p->data[p->sz].name);
        printf("请输入性别:>"); scanf("%s", p->data[p->sz].sex);
        printf("请输入年龄:>"); scanf("%d", &(p->data[p->sz].age));
        printf("请输入电话:>"); scanf("%s", p->data[p->sz].tele);
        printf("请输入地址:>"); scanf("%s", p->data[p->sz].addr);
        p->sz++;
        printf("增加联系人成功！\n");
    }
}
//展示通讯录
void Show(struct Contact* p)
{
    printf("%-5s\t%-5s\t%-5s\t%-10s\t%-5s\n",
        "姓名", "性别", "年龄", "电话", "地址");
    for (int i = 0; i < p->sz; i++)
    {
        printf("%-5s\t%-5s\t%-5d\t%-10s\t%-5s\n",
            p->data[i].name,
            p->data[i].sex,
            p->data[i].age,
            p->data[i].tele,
            p->data[i].addr);
    }
}
//删除联系人
void Del(struct Contact* p)
{
    char Name[_MAX];
    printf("请输入要删除联系人的名字:>");
    scanf("%s", Name);
    int ret = FindByName(p, Name);
    if (ret == -1)
    {
        printf("要删除的联系人不存在！\n");
    }
    else
    {
        for (int i = ret; i < p->sz - 1; i++)
        {
            p->data[i] = p->data[i + 1];
        }
        p->sz--;
        printf("删除联系人成功！\n");
    }
}
//查找联系人
void Search(struct Contact* p)
{
    char Name[_MAX];
    printf("请输入要查找联系人的名字:>");
    scanf("%s", Name);
    int ret = FindByName(p, Name);
    if (ret == -1)
    {
        printf("要查找的联系人不存在！\n");
    }
    else
    {
        printf("%-5s\t%-5s\t%-5s\t%-10s\t%-5s\n",
            "姓名", "性别", "年龄", "电话", "地址");
        printf("%-5s\t%-5s\t%-5d\t%-10s\t%-5s\n",
            p->data[ret].name,
            p->data[ret].sex,
            p->data[ret].age,
            p->data[ret].tele,
            p->data[ret].addr);
    }
}
//修改联系人
void Modify(struct Contact* p)
{
    char Name[_MAX];
    printf("请输入要修改联系人的名字:>");
    scanf("%s", Name);
    int ret = FindByName(p, Name);
    if (ret == -1)
    {
        printf("要修改的联系人不存在！\n");
    }
    else
    {
        printf("请输入姓名:>"); scanf("%s", p->data[ret].name);
        printf("请输入性别:>"); scanf("%s", p->data[ret].sex);
        printf("请输入年龄:>"); scanf("%d", &(p->data[ret].age));
        printf("请输入电话:>"); scanf("%s", p->data[ret].tele);
        printf("请输入地址:>"); scanf("%s", p->data[ret].addr);

        printf("修改联系人成功！\n");
    }
}
//排序联系人
void Sort(struct Contact* p)
{
    qsort(p->data, p->sz, sizeof(struct PeoInfo), SortByName);
    printf("排序成功！\n");
}
//销毁通讯录
void Destory(struct Contact* p)
{
    free(p->data);
    p->sz = 0;
    p->capacity = 0;
}
//退出通讯录
void Exit(struct Contact* p)
{
    assert(p);

    SaveContact(p);
    Destory(p);
    printf("退出通讯录成功！\n");
    exit(0);
}
int main()
{
    int input = 0;
    struct Contact con;
    InitContact(&con);
    void (*Menu[7])(struct Contact*) = { Exit,Add,Del,Search,Modify,Show,Sort };
    do
    {
        menu();
        printf("请输入:>");
        scanf("%d", &input);
        system("cls");
        if (input >= 0 && input <= 7)
            Menu[input](&con);
        else
            printf("输入非法，请重新输入！\n");
    } while (input);

    return 0;
}