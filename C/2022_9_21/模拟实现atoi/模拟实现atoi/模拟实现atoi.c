#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现atoi
#include<stdio.h>
#include<ctype.h>
#include<assert.h>
#include<limits.h>
enum status
{
    VALID,
    INVALID
}status = INVALID;
int my_atoi(const char* str)
{
    assert(str);

    int flag = 1;
    if (*str == '\0')
        return 0;
    while (isspace(*str))
    {
        str++;
    }
    if (*str == '+')
    {
        flag = 1;
        str++;
    }
    else if (*str == '-')
    {
        flag = -1;
        str++;
    }
    long long tmp = 0;
    while (*str)
    {
        if (isdigit(*str))
        {
            tmp = tmp * 10 + flag * (*str - '0');
            if (tmp > INT_MAX || tmp < INT_MIN)
            {
                return tmp;
            }
        }
        else
        {
            return tmp;
        }
        str++;
    }
    status = VALID;
    return tmp;
}
int main()
{
    char arr[] = "+158";
    int ret = my_atoi(arr);
    if (status == VALID)
        printf("正常转换：%d", ret);
    else if (status == INVALID)
        printf("异常转换：%d", ret);

    return 0;
}