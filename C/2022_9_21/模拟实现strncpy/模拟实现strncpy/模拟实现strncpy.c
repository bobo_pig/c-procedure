#define _CRT_SECURE_NO_WARNINGS 1
//ģ��ʵ��strncpy
#include<stdio.h>
#include<assert.h>
char* my_strncpy(char* dest, const char* src, size_t num)
{
    assert(src);

    char* ret = dest;
    while ((*dest++ = *src++) && num - 1)
    {
        num--;
    }
    return ret;
}
int main()
{
    char arr[] = "abcdf";
    char str[10];
    printf("%s", my_strncpy(str, arr, 2));

    return 0;
}