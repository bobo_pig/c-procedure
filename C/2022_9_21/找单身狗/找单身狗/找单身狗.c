#define _CRT_SECURE_NO_WARNINGS 1
//�ҵ�����
#include<stdio.h>
void get_single_dog(int arr[], int sz,
    int* dog1, int* dog2)
{
    for (int i = 0; i < sz; i++)
    {
        if (arr[i] % 2 == 0)
        {
            *dog1 ^= arr[i];
        }
        else if (arr[i] % 2 == 1)
        {
            *dog2 ^= arr[i];
        }
    }
}
int main()
{
    int arr[] = { 1,2,3,4,5,1,2,3,4,6 };
    int sz = sizeof(arr) / sizeof(arr[0]);
    int dog1 = 0;
    int dog2 = 0;
    get_single_dog(arr, sz, &dog1, &dog2);
    printf("%d %d", dog1, dog2);

    return 0;
}