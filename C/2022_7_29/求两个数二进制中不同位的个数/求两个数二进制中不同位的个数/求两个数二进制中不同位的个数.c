#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//统计二进制不同的个数
int main()
{
    int n, m, tmp;
    while (scanf("%d%d", &n, &m) != EOF)
    {
        int count = 0;
        tmp = m ^ n;
        while (tmp)
        {
            tmp &= tmp - 1;
            count++;
        }
        printf("%d\n", count);
    }

    return 0;
}