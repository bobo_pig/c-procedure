#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//统计二进制1的个数
int main()
{
    int n;
    while (scanf("%d", &n) != EOF)
    {
        int count = 0;
        while (n)
        {
            n &= (n - 1);
            count++;
        }
        printf("%d\n", count);
    }

    return 0;
}