#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//qsort的模拟
void Swap(char* buf1, char* buf2, int width)
{
    for (int i = 0; i < width; i++)
    {
        char tmp = *buf1;
        *buf1 = *buf2;
        *buf2 = tmp;
        buf1++;
        buf2++;
    }
}

void bubble_sort(void* base, size_t sz, size_t width,
    int (*cmp)(const void*, const void*))
{
    for (int i = 0; i < sz - 1; i++)
    {
        for (int j = 0; j < sz - 1 - i; j++)
        {
            if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
            {
                Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
            }
        }
    }
}

struct stu
{
    char name[20];
    int age;
};

int cmp_int(const void* e1, const void* e2)
{
    return (*(int*)e1 - *(int*)e2);
}

int cmp_char(const void* e1, const void* e2)
{
    return strcmp((char*)e1, (char*)e2);//不要写成*(char*)
}

int cmp_by_name(const void* e1, const void* e2)
{
    return strcmp(((struct stu*)e1)->name, ((struct stu*)e2)->name);
}

int cmp_by_age(const void* e1, const void* e2)
{
    return (((struct stu*)e1)->age - ((struct stu*)e2)->age);
}

void test1(void)
{
    int arr[] = { 4,5,3,9,8,2,1,7,0,6 };
    int sz = sizeof(arr) / sizeof(arr[0]);
    bubble_sort(arr, sz, sizeof(arr[0]), cmp_int);
    for (int i = 0; i < sz; i++)
        printf("%d", arr[i]);
    printf("\n");
}

void test2(void)
{
    char arr[] = { 'b','o','b','o','l','j' };
    int sz = sizeof(arr) / sizeof(arr[0]);
    bubble_sort(arr, sz, sizeof(arr[0]), cmp_char);
    for (int i = 0; i < sz; i++)
        printf("%s\n", arr);
}

void test3(void)
{
    struct stu s[2] = { {"bobo",27},{"jiujiu",28} };
    int sz = sizeof(s) / sizeof(s[0]);
    bubble_sort(s, sz, sizeof(s[0]), cmp_by_name);
    bubble_sort(s, sz, sizeof(s[0]), cmp_by_age);
    struct stu* p = s;
    for (p = s; p < &s[2]; p++)
        printf("%s,%d\n", (*p).name, (*p).age);
}

int main()
{
    test1();
    test2();
    test3();
    return 0;
}