#include"game.h"

//实现游戏
void game()
{
    //棋盘
    char ret;
    char board[ROW][COL];
    init_board(board, ROW, COL);
    display_board(board, ROW, COL);
    //开始下棋
    while (1)
    {
        //玩家下棋
        player_move(board, ROW, COL);
        display_board(board, ROW, COL);
        //判断输赢情况
        ret = is_win(board, ROW, COL);
        if (ret != 'C')
            break;
        //电脑下棋
        computer_move(board, ROW, COL);
        display_board(board, ROW, COL);
        //判断输赢情况
        ret = is_win(board, ROW, COL);
        if (ret != 'C')
            break;
    }
    if ('*' == ret)
        printf("玩家赢了!\n");
    else if ('#' == ret)
        printf("电脑赢了!\n");
    else if ('Q' == ret)
        printf("平局!\n");
}

//判断棋盘是否满了
int is_full(char board[ROW][COL], int row, int col)
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            if (' ' == board[i][j])
                return 0;
        }
    }
    return 1;
}

//判断输赢、平局、继续游戏
char is_win(char board[ROW][COL], int row, int col)
{
    //判断输赢
    for (int i = 0; i < row; i++)
    {
        if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != ' ')
            return board[i][0];
    }
    for (int i = 0; i < col; i++)
    {
        if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != ' ')
            return board[0][i];
    }
    if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != ' ')
        return board[1][1];
    if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')
        return board[1][1];
    //判断平局
    if (is_full(board, row, col) == 1)
        return 'Q';
    //游戏继续
    return 'C';
}

//电脑下棋
void computer_move(char board[ROW][COL], int row, int col)
{
    printf("电脑输入:>\n");
    int x, y;
    while (1)
    {
        x = rand() % row;
        y = rand() % col;
        if (board[x][y] == ' ')
        {
            board[x][y] = '#';
            break;
        }
    }
}

//玩家下棋
void player_move(char board[ROW][COL], int row, int col)
{
    int x = 0;
    int y = 0;
    printf("玩家输入:>\n");
    printf("请输入坐标:>");
    while (1)
    {
        scanf("%d %d", &x, &y);
        if (x >= 1 && x <= ROW && y >= 1 && y <= COL)
        {
            if (board[x - 1][y - 1] == ' ')
            {
                board[x - 1][y - 1] = '*';
                break;
            }
            else
                printf("输入非法，请重新输入！\n");
        }
        else
            printf("输入非法，请重新输入！\n");
    }
}

//打印棋盘
void display_board(char board[ROW][COL], int row, int col)
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf(" %c ", board[i][j]);
            if (j < col - 1)
                printf("|");
        }
        printf("\n");
        if (i < row - 1)
        {
            for (int j = 0; j < col; j++)
            {
                printf("---");
                if (j < col - 1)
                    printf("|");
            }
            printf("\n");
        }
    }
}

//初始化棋盘
void init_board(char board[ROW][COL], int row, int col)
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
            board[i][j] = ' ';
    }
}

//打印菜单
void menu()
{
    printf("*****************\n");
    printf("****1. Play  ****\n");
    printf("****0. Exit  ****\n");
    printf("*****************\n");
}