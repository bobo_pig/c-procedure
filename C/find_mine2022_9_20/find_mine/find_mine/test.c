//扫雷游戏
#include "game.h"

void game()
{
    char mine[ROWS][COLS] = { 0 };//储存雷
    char show[ROWS][COLS] = { 0 };//展示周围雷个数
    char contrast[ROWS][COLS] = { 0 };//把排查过的坐标进行标记
    //初始化棋盘
    init_board(mine, ROWS, COLS, '0');
    init_board(show, ROWS, COLS, '*');
    //打印棋盘
    display_board(show, ROW, COL);
    //布置雷
    set_mine(mine, ROW, COL);
    //排雷
    find_mine(mine, show, contrast, ROW, COL);
}

void menu()
{
    printf("****************\n");
    printf("****1. play ****\n");
    printf("****0. exit ****\n");
    printf("****************\n");
    //printf("嘤嘤嘤~共有%d颗雷，好害怕！\n", EASY_COUNT);
}

int main()
{
    srand((unsigned int)time(NULL));
    int input = 0;
    do
    {
        menu();
        printf("小伙子请选择:>");
        scanf("%d", &input);
        system("cls");
        switch (input)
        {
        case 1:
            game();
            break;
        case 0:
            printf("不玩了！回家找妈妈~\n");
            break;
        default:
            printf("笨啊，这都能输错，请重新输入！\n");
            break;
        }
    } while (input);
    return 0;
}