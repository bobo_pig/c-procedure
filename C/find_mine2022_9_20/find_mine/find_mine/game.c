#include "game.h"

//统计show中'*'的数量，实现游戏赢的功能
int count_mine(char show[ROWS][COLS], int row,int col)
{
    int count = 0;
    for (int i = 1; i <= row; i++)
    {
        for (int j = 1; j <= col; j++)
        {
            if (show[i][j] == '*')
                count++;
        }
    }
    return count;
}

//递归扩展排雷
void Extend_board(char mine[ROWS][COLS], char show[ROWS][COLS], char contrast[ROWS][COLS], int x, int y)
{
    if (get_mine_count(mine, x, y) == 0)
        //get_mine_count函数返回存储地雷信息的mine数组中x y位置周围一圈（8个位置）的地雷总和
    {
        for (int i = x - 1; i <= x + 1; i++)  //不得超出棋盘大小
        {
            for (int j = y - 1; j <= y + 1; j++) //不得超出棋盘大小
            {
                show[i][j] = (get_mine_count(mine, i, j)+'0'); //将该位置一圈地雷总和信息传递到show（）函数
                if (show[i][j] == '0' && contrast[i][j] != '$') //如果该位置周围没有地雷且没有被标记
                {
                    contrast[i][j] = '$'; //将该位置标记
                    Extend_board(mine, show, contrast, i, j); //调用递归函数再次进行判断
                }
            }
        }
    }
    else
        show[x][y] = (get_mine_count(mine, x, y) + '0');
    return;
}

//统计雷的数量
int get_mine_count(char mine[ROWS][COLS], int x, int y)
{
    return (mine[x - 1][y] +
        mine[x - 1][y - 1] +
        mine[x][y - 1] +
        mine[x + 1][y - 1] +
        mine[x + 1][y] +
        mine[x + 1][y + 1] +
        mine[x][y + 1] +
        mine[x - 1][y + 1] - 8 * '0');
}
//排雷
void find_mine(char mine[ROWS][COLS], char show[ROWS][COLS],char contrast[ROWS][COLS], int row, int col)
{
    int x, y;
    int win = 0;
    while (win != EASY_COUNT)
    {
        printf("请输入排雷的坐标:>");
        scanf("%d %d", &x, &y);
        system("cls");
        if (x >= 1 && x <= row && y >= 1 && y <= col)
        {
            if (show[x][y] == '*')
            {
                if (mine[x][y] == '1')
                {
                    printf("很遗憾，你被炸毛了！\n");
                    break;
                }
                else
                {
                    contrast[x][y] = '$';
                    Extend_board(mine, show, contrast, x, y);
                    display_board(show, ROW, COL);
                    win = count_mine(show, ROW, COL);
                }
            }
            else
            {
                printf("该位置坐标已经排查过啦，请重新输入！\n");
                display_board(show, ROW, COL);
            }
        }
        else
        {
            printf("无效的坐标哦~请重新输入！\n");
            display_board(show, ROW, COL);
        }

    }
    if (win == EASY_COUNT)
        printf("恭喜您，扫雷成功！奖励一拖鞋~\n");
}
//布置雷
void set_mine(char mine[ROWS][COLS], int row, int col)
{
    int count = EASY_COUNT;
    while (count)
    {
        int x = rand() % row + 1;
        int y = rand() % col + 1;
        if (mine[x][y] == '0')
        {
            mine[x][y] = '1';
            count--;
        }
    }
}
//打印棋盘
void display_board(char board[ROWS][COLS], int row, int col)
{
    for (int j = 0; j <= col; j++)
        printf("%d ", j);
    printf("\n");
    for (int i = 1; i <= row; i++)
    {
        printf("%d ", i);
        for (int j = 1; j <= col; j++)
        {
            printf("%c ", board[i][j]);
        }
        printf("\n");
    }
}
//初始化棋盘
void init_board(char board[ROWS][COLS], int row, int col, char set)
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
            board[i][j] = set;
    }
}