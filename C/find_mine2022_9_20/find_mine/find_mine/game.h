#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ROW 9
#define COL 9
#define ROWS ROW+2
#define COLS COL+2
#define EASY_COUNT 10

//初始化棋盘
void init_board(char board[ROWS][COLS], int row, int col, char set);
//打印棋盘
void display_board(char board[ROWS][COLS], int row, int col);
//布置雷
void set_mine(char mine[ROWS][COLS], int row, int col);
//排雷
void find_mine(char mine[ROWS][COLS], char show[ROWS][COLS], char contrast[ROWS][COLS], int row, int col);
//统计雷的数量
int get_mine_count(char mine[ROWS][COLS], int x, int y);
//自动扩展排雷
void Extend_board(char mine[ROWS][COLS], char show[ROWS][COLS], char contrast[ROWS][COLS], int x, int y, int* win);
//统计show中'*'的数量，实现游戏赢的功能
int count_mine(char show[ROWS][COLS], int row, int col);