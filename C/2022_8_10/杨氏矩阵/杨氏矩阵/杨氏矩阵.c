#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//杨氏矩阵3.0
void search(int arr[3][3], int* r, int* c, int k)
{
    int i = 0;
    int j = *c - 1;
    int flag = 0;
    while (i < *r && j >= 0)
    {
        if (arr[i][j] < k)
        {
            i++;
        }
        else if (arr[i][j] > k)
        {
            j--;
        }
        else
        {
            flag = 1;
            *r = i;
            *c = j;
            break;
        }
    }
    if (flag == 0)
    {
        *r = -1;
        *c = -1;
    }
}
int main()
{
    int arr[3][3];
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
            scanf("%d", &arr[i][j]);
    }
    int k;
    scanf("%d", &k);
    int x = 3;
    int y = 3;
    search(arr, &x, &y, k);
    if (x == -1 && y == -1)
    {
        printf("找不到\n");
    }
    else
    {
        printf("下标是(%d,%d)\n", x, y);
    }

    return 0;
}