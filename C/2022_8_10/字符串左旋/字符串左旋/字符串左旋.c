#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//�ַ�������2.0
void reverse(char* left, char* right)
{
    while (left < right)
    {
        char tmp;
        tmp = *left;
        *left = *right;
        *right = tmp;
        left++;
        right--;
    }
}
int main()
{
    char arr[] = "ABCDEFG";
    int len = strlen(arr);
    int k;
    while (~scanf("%d", &k))
    {
        k %= len;
        reverse(arr, arr + k - 1);
        reverse(arr + k, arr + len - 1);
        reverse(arr, arr + len - 1);
        printf("%s\n", arr);
    }
    return 0;
}