#include <stdio.h>
//交换两个整数
void exa(int* a, int* b)
{
    int tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

int main()
{
    int a = 10;
    int b = 20;
    exa(&a, &b);
    printf("a=%d\nb=%d", a, b);

    return 0;
}