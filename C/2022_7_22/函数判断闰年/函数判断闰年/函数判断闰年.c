#include <stdio.h>//函数判断闰年
int judge(int x)
{
    if ((x % 4 == 0 && x % 100 != 0) || (x % 400 == 0))
        return 1;
    else
        return 0;
}

int main()
{
    int year, n;
    scanf_s("%d", &year);
    n = judge(year);
    if (1 == n)
        printf("%d是闰年", year);
    else
        printf("%d不是闰年", year);

    return 0;
}