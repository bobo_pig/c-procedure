#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
//ģ��strstr
char* my_strstr(const char* arr, const char* str)
{
    assert(arr && str);

    const char* s1 = arr;
    const char* s2 = str;
    const char* p = arr;
    while (*p != '\0')
    {
        s1 = p;
        s2 = str;
        while (*s1 == *s2)
        {
            s1++;
            s2++;
        }
        if (*s2 == '\0')
        {
            return (char*)p;
        }
        p++;
    }
    return NULL;
}

int main()
{
    char arr[] = "abbbcdbbc";
    char str[] = "bbc";
    char* ret = my_strstr(arr, str);
    if (ret)
        printf("%s", ret);
    else
        printf("������");

    return 0;
}
