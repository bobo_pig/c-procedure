#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
//ģ��strcmp
int my_strcmp(const char* arr, const char* str)
{
    assert(arr && str);

    while (*arr == *str)
    {
        if (*arr == '\0')
        {
            return 0;
        }
        arr++;
        str++;
    }
    return *arr - *str;
}

int main()
{
    char arr[] = "abc";
    char str[] = "abcd";
    int ret = my_strcmp(arr, str);
    if (ret > 0)
        printf("arr > str");
    else if (ret < 0)
        printf("arr < str");
    else
        printf("arr = str");

    return 0;
}