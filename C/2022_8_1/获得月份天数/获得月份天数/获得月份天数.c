#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int is_leap_year(int y)
{
    if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
        return 1;
    else
        return 0;
}

int main()
{
    int y, m;
    int days[13] = { 0,31,28,31,30,31,30
                 ,31,31,30,31,30,31 };
    while (scanf("%d%d", &y, &m) != EOF)
    {
        int day = days[m];
        if (is_leap_year(y) && 2 == m)
            day++;
        printf("%d\n", day);
    }
    return 0;
}