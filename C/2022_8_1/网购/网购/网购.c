#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
    float price;
    int m, d;
    int flag;
    while (scanf("%f %d %d %d", &price, &m, &d, &flag) != EOF)
    {
        if (11 == m && 11 == d)
            price *= 0.7;
        else if (12 == m && 12 == d)
            price *= 0.8;
        if (flag)
            price -= 50;
        if (price < 0.0)
            price = 0.0;
        printf("%.2f", price);
    }
    return 0;
}