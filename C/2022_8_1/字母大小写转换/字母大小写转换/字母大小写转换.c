#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<ctype.h>

int main()
{
    char n;
    while (scanf("%c", &n) != EOF)
    {
        if (isupper(n))
            printf("%c\n", tolower(n));
        else if (islower(n))
            printf("%c\n", toupper(n));
    }
    return 0;
}