#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<ctype.h>

int main()
{
    char n;
    while (scanf("%c", &n) != EOF)
    {
        if (isalpha(n))
            printf("%c is an alphabet.\n", n);
        else
            printf("%c is not an alphabet.\n", n);
        getchar();
    }

    return 0;
}