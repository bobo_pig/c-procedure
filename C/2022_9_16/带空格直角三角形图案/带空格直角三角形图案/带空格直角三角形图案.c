#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//带空格直角三角形图案
int main()
{
    int n = 0;
    while (~scanf("%d", &n))
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n - 1 - i; j++)
                printf("  ");
            for (int k = 0; k < i + 1; k++)
                printf("* ");
            printf("\n");
        }
    }

    return 0;
}