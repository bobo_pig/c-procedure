#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
    int a, b, i;
    int sum = 0;

    scanf("%d", &a);
    while (scanf("%d", &b) != EOF)
    {
        sum += b;
    }
    printf("%d", sum);

    return 0;
}