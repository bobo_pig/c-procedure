#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//递归：输入一个非负整数，返回组成它的数字之和
int DigitSum(int n)
{
    if (n > 9)
        return n % 10 + DigitSum(n / 10);
    else
        return n;
}

int main()
{
    int n;
    while (scanf("%d", &n) != EOF)
    {
        int result = DigitSum(n);
        printf("%d\n", result);
    }
    return 0;
}