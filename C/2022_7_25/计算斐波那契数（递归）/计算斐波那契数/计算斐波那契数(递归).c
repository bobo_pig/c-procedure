#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//斐波那契数(递归)

int fac(int n)
{
    if (n > 2)
        return fac(n - 1) + fac(n - 2);
    else
        return 1;
}

int main()
{
    int n;
    while (~scanf("%d", &n))
    {
        int result = fac(n);
        printf("第%d个斐波那契数是%d\n", n, result);
    }
    return 0;
}