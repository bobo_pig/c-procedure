#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//字符串逆序(递归)
void reverse_string(char* str)
{
    int len = strlen(str);//下次进入函数时还会赋值
    if (str < (str + len - 1))
    {
        char temp = *str;
        *str = *(str + len - 1);
        *(str + len - 1) = '\0';
        reverse_string(str + 1);
        *(str + len - 1) = temp;
    }
}

int main()
{
    char arr[] = {0};
    while (scanf("%s", arr) != EOF)
    {
        reverse_string(arr);
        printf("%s\n", arr);
    }
    return 0;
}