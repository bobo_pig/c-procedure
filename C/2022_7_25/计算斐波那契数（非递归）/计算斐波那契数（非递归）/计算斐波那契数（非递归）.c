#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//斐波那契数(非递归)
int main()
{
    int n;
    while (~scanf("%d", &n))
    {
        if (n < 3)
            printf("第%d个斐波那契数是1\n", n);
        else
        {
            int c = 0;
            int a = 1;
            int b = 1;
            for (int i = 3; i <= n; i++)
            {
                c = a + b;
                a = b;
                b = c;
            }
            printf("第%d个斐波那契数是%d\n", n, c);
        }
    }
    return 0;
}