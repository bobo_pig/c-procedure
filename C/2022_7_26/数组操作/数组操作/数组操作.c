#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//��������

void init(int* str)
{
    for (int i = 0; i < 10; i++)
        str[i] = i;
}

void print(int* str)
{
    for (int i = 0; i < 10; i++)
        printf("%d", str[i]);
    printf("\n");
}

void reverse(int* str, int sz)
{
    int tmp;
    int* left = str;
    int* right = str + sz - 1;
    while (left < right)
    {
        tmp = *left;
        *left = *right;
        *right = tmp;
        left++;
        right--;
    }
}

int main()
{
    int arr[10];
    int sz = sizeof(arr) / sizeof(arr[0]);
    init(arr);
    print(arr);
    reverse(arr, sz);
    print(arr);

    return 0;
}