#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//调整奇数偶数问题
int main()
{
    int arr[] = { 6,4,2,1,8,7,3,9,0 };
    int sz = sizeof(arr) / sizeof(arr[0]);
    int left = 0;
    int right = sz - 1;
    while (left < right)
    {
        if (arr[left] % 2 == 0 && arr[right] % 2 == 1)
        {
            int tmp = arr[left];
            arr[left] = arr[right];
            arr[right] = tmp;
            left++;
            right--;
        }
        if (arr[left] % 2 == 1)
            left++;
        if (arr[right] % 2 == 0)
            right--;
    }
    for (int i = 0; i < sz - 1; i++)
        printf("%d", arr[i]);

    return 0;
}