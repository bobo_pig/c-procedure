#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//�ж��Ƿ���������
int judge_sorted(int* arr, int sz)
{
    int count1 = 0;
    int count2 = 0;
    for (int i = 0; i < sz - 1; i++)
    {
        if (arr[i] <= arr[i + 1])
            count1++;
        if (arr[i] >= arr[i + 1])
            count2++;
    }
    if ((count1 == (sz - 1)) || (count2 == (sz - 1)))
        return 1;
    else
        return 0;
}

int main()
{
    int n;
    scanf("%d", &n);
    int arr[50];
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }
    int sz = sizeof(arr) / sizeof(arr[0]);
    int result = judge_sorted(arr, sz);
    if (result)
        printf("sorted");
    else
        printf("unsorted");

    return 0;
}