#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//最大最小差值
int main()
{
    int min = 100;
    int max = 0;
    int n, grade;
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &grade);
        if (min > grade)
            min = grade;
        if (max < grade)
            max = grade;
    }
    printf("%d", max - min);

    return 0;
}