#include <stdio.h>
#include <math.h>//打印0~n之间的所有素数
int main()
{
    int n, i, j;
    scanf("%d", &n);
    for (i = 0; i <= n; i++)
    {
        for (j = 2; j < sqrt(i); j++)
        {
            if ((i % j) == 0)
                break;
        }
        if ((i % j) != 0)
            printf("%d ", i);
    }
    return 0;
}