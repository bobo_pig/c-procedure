#include <stdio.h>
//从大到小输出
int main()
{
    int i, j, n, temp;

    int arr[10] = { 0 };
    scanf("%d %d %d", &arr[0], &arr[1], &arr[2]);
    for (i = 0; i < (sizeof(arr) / 4 - 1); i++)
    {
        for (j = 0; j < (sizeof(arr) / 4 - 1); j++)
        {
            if (arr[j] < arr[j + 1])
            {
                temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    for (n = 0; n < 3; n++)
        printf("%d", arr[n]);

    return 0;
}