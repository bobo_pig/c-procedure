#include <stdio.h>//求10个整数中最大值
int main()
{
    int arr[10];
    for (int i = 0; i < 10; i++)
    {
        scanf("%d", &arr[i]);
    }
    int max = arr[0];
    for (int j = 1; j < 10; j++)
    {
        if (arr[j] > max)
            max = arr[j];
    }
    printf("%d", max);
    return 0;
}