#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//�ַ�������
void reverse(char* str, int sz)
{
    char* left = str;
    char* right = str + sz - 1;
    while (left < right)
    {
        char tmp = *left;
        *left = *right;
        *right = tmp;
        left++;
        right--;
    }
}

int main()
{
    char arr[100];
    scanf("%[^\n]", arr);
    int sz = strlen(arr);
    reverse(arr, sz);
    printf("%s", arr);

    return 0;
}