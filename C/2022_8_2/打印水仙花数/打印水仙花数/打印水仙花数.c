#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>
//打印自幂数
int main()
{
    for (int n = 0; n < 100000; n++)
    {
        int tmp = n;//因为以下的计算会改变n的值，则用tmp代替
        int i = 1;
        while (tmp /= 10)
        {
            i++;
        }
        int sum = 0;
        tmp = n;//重新把tmp赋予n的值
        while (tmp)
        {
            sum += (int)pow(tmp % 10, i);
            tmp /= 10;
        }
        if (sum == n)
        {
            printf("%d\n", n);
        }
    }
    return 0;
}