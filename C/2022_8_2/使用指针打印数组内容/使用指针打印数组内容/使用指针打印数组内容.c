#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//使用指针打印arr数组内容
void print(int* str)
{
    for (int i = 0; i < 5; i++)
    {
        printf("%d", *(str + i));
    }
}

int main()
{
    int arr[5] = { 1,2,3,4,5 };
    print(arr);

    return 0;
}