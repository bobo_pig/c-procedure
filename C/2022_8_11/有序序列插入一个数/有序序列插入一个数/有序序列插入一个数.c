#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//有序序列插入一个数
int main()
{
    int n;
    scanf("%d", &n);
    int arr[n + 1];
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }
    int k;
    scanf("%d", &k);
    int j = n - 1;
    for (; j >= 0; j--)
    {
        if (arr[j] > k)
            arr[j + 1] = arr[j];
        else
            break;
    }
    arr[j + 1] = k;
    for (int i = 0; i < n + 1; i++)
        printf("%d ", arr[i]);
    return 0;
}