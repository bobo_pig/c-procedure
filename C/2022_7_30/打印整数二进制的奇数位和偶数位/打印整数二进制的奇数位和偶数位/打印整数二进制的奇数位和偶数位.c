#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//打印二进制奇偶数位
void odd(int n)
{
    for (int i = 30; i >= 0; i -= 2)
        printf("%d", (n >> i) & 1);
}

void even(int n)
{
    for (int i = 31; i >= 1; i -= 2)
        printf("%d", (n >> i) & 1);
}

int main()
{
    int n;
    while (scanf("%d", &n) != EOF)
    {
        odd(n);
        printf("\n");
        even(n);
    }
    return 0;
}