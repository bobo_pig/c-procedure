#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
    int n, m;
    while (scanf("%d", &n) != EOF)
    {
        int arr[20];
        for (int i = 0; i < n; i++)
            scanf("%d", &arr[i]);

        int sz = sizeof(arr) / sizeof(arr[0]);
        scanf("%d", &m);

        int j = 0;
        for (int i = 0; i < sz; i++)
        {

            if (arr[i] != m)
            {
                arr[j] = arr[i];
                j++;
            }
        }
        for (int i = 0; i < j; i++)
            printf("%d ", arr[i]);
    }
    return 0;
}