#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int walk(int n)
{
    if (1 == n)
        return 1;
    else if (2 == n)
        return 2;
    else
        return walk(n - 1) + walk(n - 2);
}

int main()
{
    int n;
    while (scanf("%d", &n) != EOF)
    {
        int sum = walk(n);
        printf("%d\n", sum);
    }
    return 0;
}