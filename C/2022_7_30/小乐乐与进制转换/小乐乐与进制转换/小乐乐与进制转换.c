#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void six(int n)
{
    if (n > 5)
        six(n / 6);
    printf("%d", n % 6);
}

int main()
{
    int n;
    while (scanf("%d", &n) != EOF)
    {
        six(n);
    }

    return 0;
}