#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
    long long n, m, n2, m2;
    while (scanf("%lld %lld", &n, &m) != EOF)//记得输入也要改成%lld
    {
        n2 = n;
        m2 = m;
        long long c = 0;
        while (1)
        {
            c = n2 % m2;
            if (0 == c)
                break;
            else
            {
                n2 = m2;
                m2 = c;
            }
        }
        long long max = m2;
        long long min = m * n / max;
        printf("%lld\n", (max + min));
    }
    return 0;
}